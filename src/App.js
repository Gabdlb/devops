import React from 'react';

function App() {
  const value = 'Amogus';
  return <div>Hello {value}</div>;
}

export default App;
